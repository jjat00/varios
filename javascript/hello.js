//Metodos y propiedades para los arreglos
//var amigos = ["Carlos", "Cesar", "Alex"];
//var amigos2 = ["Maria"];
//
//amigos[0] = "Arturo";
//amigos[amigos.length] = "Jjat"
//var amigos3 = amigos.concat(amigos2);
//var amigos4 = amigos.join(" : ");
//var amigos5 = amigos.sort(); //para ordenar en orden alfabético
//
//console.log(amigos);
//console.log(amigos3);
//console.log(amigos4);
//console.log(amigos5);

//Condicionales
var nombre = "Jjat",
    edad = 22;

if (nombre == "Jjat") {
    console.log("Bienvenido " + nombre);
} else {
    console.log("Bienvenido anonimo");
}