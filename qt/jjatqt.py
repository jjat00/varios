import sys
from PyQt5 import QtGui, uic, QtWidgets

class MyWindow(QtWidgets.QDialog):
    def __init__(self):
        super(MyWindow, self).__init__()
        uic.loadUi('qt/hello.ui', self)
        self.show()

        self.clear_button.clicked.connect(self.clear_button_clicked)

    def clear_button_clicked(self): 
        self.spinCel.setValue(int(7))
        self.labelCel.setText("hello Jjat")
        

if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    window = MyWindow()
    sys.exit(app.exec_())